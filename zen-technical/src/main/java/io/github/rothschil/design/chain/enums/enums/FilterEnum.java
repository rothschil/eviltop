package io.github.rothschil.design.chain.enums.enums;

import io.github.rothschil.design.chain.enums.entity.FilterEntity;
import io.github.rothschil.design.chain.enums.handler.*;

/**
 * @author <a href="https://github.com/rothschil">Sam</a>
 * @date 2022/8/4 - 23:37
 * @since 1.0.0
 */
public enum FilterEnum {

    FIRST_HANDLER(new FilterEntity(1, "首要处理环节", FirstFilterHandler.class.getName(), null, 2)),
    SECOND_HANDLER(new FilterEntity(2, "二次处理环节", SecondFilterHandler.class.getName(), 1, 3)),
    THIRD_HANDLER(new FilterEntity(3, "三次处理环节", ThirdFilterHandler.class.getName(), 2, null));

    FilterEntity filterEntity;

    public FilterEntity getGameEntity() {
        return filterEntity;
    }

    FilterEnum(FilterEntity filterEntity) {
        this.filterEntity = filterEntity;
    }
}
