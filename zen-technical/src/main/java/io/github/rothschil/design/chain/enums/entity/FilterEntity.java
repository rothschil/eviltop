package io.github.rothschil.design.chain.enums.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author <a href="https://github.com/rothschil">Sam</a>
 * @date 2022/8/5 - 11:36
 * @since 1.0.0
 */
@Data
@AllArgsConstructor
public class FilterEntity {
    /**
     * handlerId
     */
    private Integer handlerId;

    /**
     * 处理器名称
     */
    private String name;

    /**
     * 处理器 包名 + 类名
     */
    private String conference;

    /**
     * 上一个处理器
     */
    private Integer preHandlerId;

    /**
     * 下一个处理器
     */
    private Integer nextHandlerId;
}
