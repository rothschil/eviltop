package io.github.rothschil.design.iter;

/**
 * @author <a href="https://github.com/rothschil">Sam</a>
 * @date 2022/8/6 - 13:20
 * @since 1.0.0
 */
public class FilterService implements Container{

    public static final String[] UN_FILER = {"CharsetFilter" , "LoginFilter" ,"RuleFilter" , "MvcFilter"};


    @Override
    public Iterator getIterator() {
        return new FilterIterator();
    }

    class FilterIterator implements Iterator<String>{
        int index;

        @Override
        public boolean hasNext() {
            if(index< UN_FILER.length){
                return true;
            }
            return false;
        }

        @Override
        public String next() {
            if(this.hasNext()){
                return UN_FILER[index++];
            }
            return null;
        }
    }
}
