package io.github.rothschil.design.iter;

/**
 * 类似集合
 *
 * @author <a href="https://github.com/rothschil">Sam</a>
 * @date 2022/8/6 - 13:12
 * @since 1.0.0
 */
public interface Container<T> {

    /**
     * 迭代器
     *
     * @return Iterator<T>
     * @author <a href="https://github.com/rothschil">Sam</a>
     * @date 2022/8/6-13:13
     **/
    Iterator<T> getIterator();
}
