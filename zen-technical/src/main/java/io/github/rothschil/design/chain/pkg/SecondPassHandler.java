package io.github.rothschil.design.chain.pkg;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="https://github.com/rothschil">Sam</a>
 * @date 2022/8/4 - 22:58
 * @since 1.0.0
 */
@Slf4j
public class SecondPassHandler extends AbstractHandler {

    static final int SECOND_SCOPE = 80;

    @Override
    public int handler() {
        int score = second();
        if(score>=SECOND_SCOPE){
            log.error("处理完毕，交由下一环节处理");
            if(this.next != null){
                return this.next.handler();
            }
        }else{
            log.error("处理不了，交由下一环节处理");
        }
        return score;
    }

    private int second(){
        return SECOND_SCOPE;
    }
}
