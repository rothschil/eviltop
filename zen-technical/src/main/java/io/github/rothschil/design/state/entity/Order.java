package io.github.rothschil.design.state.entity;

import io.github.rothschil.design.state.State;
import lombok.Data;

@Data
public class Order {
    private Long orderId;
    private String orderTranCd;
    private State state;


    public Order(){

    }

    public Order(Long orderId,String orderTranCd){
        this.orderId=orderId;
        this.orderTranCd=orderTranCd;
    }

}
