package io.github.rothschil.design.chain.enums.dao;

import io.github.rothschil.design.chain.enums.entity.FilterEntity;

/**
 * @author <a href="https://github.com/rothschil">Sam</a>
 * @date 2022/8/5 - 12:41
 * @since 1.0.0
 */
public interface FilterDao {
    /**
     * 根据 handlerId 获取配置项
     * @param handlerId
     * @return
     */
    FilterEntity getGameEntity(Integer handlerId);

    /**
     * 获取第一个处理者
     * @return
     */
    FilterEntity getFirstGameEntity();
}
