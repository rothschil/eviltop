package io.github.rothschil.design.template;

import io.github.rothschil.common.utils.StringUtils;

/**
 * @author <a href="https://github.com/rothschil">Sam</a>
 * @date 2022/8/5 - 12:37
 * @since 1.0.0
 */
public abstract class UserService {

    /** 校验密码
     * @author <a href="https://github.com/rothschil">Sam</a>
     * @date 2022/8/5-12:58
     * @param userId
     * @param password
     * @return boolean
     **/
    public final boolean verifyPassWord(String userId,String password) {
        String userPassWord = getById(userId);
        return userPassWord.equalsIgnoreCase(password);
    }

    private String getById(String userId) {
        //  1、getByCache
        String userPassWord = getCache(userId);
        //  2、缓存没有，从数据库中获取
        if(StringUtils.isEmpty(userPassWord)){
            userPassWord =readDb(userId);
        }
        //  3、写入缓存
        if(StringUtils.isEmpty(userPassWord)){
            putCache(userId,userPassWord);
        }
        return userPassWord;
    }

    protected abstract String getCache(String userId) ;

    protected abstract String putCache(String userId,String userPassWord) ;

    private String readDb(String userId) {
        return "";
    }
}
