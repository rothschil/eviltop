package io.github.rothschil.design.iter;

/**
 * 迭代器 顶层接口
 *
 * @author <a href="https://github.com/rothschil">Sam</a>
 * @date 2022/8/6 - 13:10
 * @since 1.0.0
 */
public interface Iterator<T> {

    /**
     * 判断是否存在下一个元素
     *
     * @return boolean
     * @author <a href="https://github.com/rothschil">Sam</a>
     * @date 2022/8/6-13:15
     **/
    boolean hasNext();

    /**
     * 获取下一个元素
     *
     * @return T
     * @author <a href="https://github.com/rothschil">Sam</a>
     * @date 2022/8/6-13:15
     **/
    T next();
}
