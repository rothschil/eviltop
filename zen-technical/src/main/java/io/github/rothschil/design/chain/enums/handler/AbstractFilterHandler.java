package io.github.rothschil.design.chain.enums.handler;

/**
 * 抽线 Handler
 *
 * @author <a href="https://github.com/rothschil">Sam</a>
 * @date 2022/8/4 - 22:55
 * @since 1.0.0
 */
public abstract class AbstractFilterHandler {

    /**
     * 定义下一用当前抽象类来接收
     */
    protected AbstractFilterHandler next;

    public void setNext(AbstractFilterHandler next) {
        this.next = next;
    }

    public abstract int handler();
}
