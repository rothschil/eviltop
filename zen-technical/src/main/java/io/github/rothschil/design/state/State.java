package io.github.rothschil.design.state;

import io.github.rothschil.design.state.entity.Order;

public interface State {

    /** 变更状态
     * @author <a href="https://github.com/rothschil">Sam</a>
     * @date 2022/8/5-14:09
     * @param order
     **/
    void doAction(Order order);

    /** 执⾏⾏为
     * @author <a href="https://github.com/rothschil">Sam</a>
     * @date 2022/8/5-14:09
     **/
    void execute();

}
