package io.github.rothschil.design.state;

import io.github.rothschil.design.state.entity.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PaymentConfirmNoticeBehavior implements State{

    @Override
    public void doAction(Order order) {
        log.warn("订单支付");
        order.setState(this);
    }

    @Override
    public void execute() {
        log.warn("通知商家发货");
        log.warn("通知减库存");
        log.warn("通知积分新增");
    }
}
