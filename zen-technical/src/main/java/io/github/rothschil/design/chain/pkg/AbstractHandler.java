package io.github.rothschil.design.chain.pkg;

/**
 * @author <a href="https://github.com/rothschil">Sam</a>
 * @date 2022/8/4 - 22:55
 * @since 1.0.0
 */
public abstract class AbstractHandler {
    /**
     * 定义下一用当前抽象类来接收
     */
    protected AbstractHandler next;

    public void setNext(AbstractHandler next) {
        this.next = next;
    }

    public abstract int handler();
}
