package io.github.rothschil.design.state;

import io.github.rothschil.design.state.entity.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PaymentCancelNoticeBehavior implements State{

    @Override
    public void doAction(Order order) {
        log.warn("订单取消支付");
        order.setState(this);
    }

    @Override
    public void execute() {
        log.warn("订单取消，执行库存回滚");
        log.warn("订单取消，执行退款");
        log.warn("订单取消，回退积分收益");
    }
}
