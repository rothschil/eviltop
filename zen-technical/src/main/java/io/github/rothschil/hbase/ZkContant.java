package io.github.rothschil.hbase;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ZkContant {

    public static String zookeeperPort="2181";
    public static String zookeeperQuorum="134.129.98.46,134.129.98.47,134.129.98.48";

    public String getZookeeperPort() {
        return "2181";
    }

//    @Value("${hbaseClient.zookeeperPort}")
    public void setZookeeperPort(String zookeeperPort) {
        ZkContant.zookeeperPort = zookeeperPort;
    }

    public String getZookeeperQuorum() {
        return "134.129.98.46,134.129.98.47,134.129.98.48";
    }

//    @Value("${hbaseClient.zookeeperQuorum}")
    public void setZookeeperQuorum(String zookeeperQuorum) {
        ZkContant.zookeeperQuorum = zookeeperQuorum;
    }

}
