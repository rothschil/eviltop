//package io.github.rothschil.hbase;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.hadoop.conf.Configuration;
//import org.apache.hadoop.hbase.Cell;
//import org.apache.hadoop.hbase.CellUtil;
//import org.apache.hadoop.hbase.HBaseConfiguration;
//import org.apache.hadoop.hbase.TableName;
//import org.apache.hadoop.hbase.client.*;
//import org.apache.hadoop.hbase.util.Bytes;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//
//@Slf4j
//public class HbaseUtil {
//
//    private static Configuration configuration = null;
//    private static Connection con = null;
//
//    static {
//        try {
//            String clan_clientPort = "2181";
//            String clan_quorum = "134.129.98.46,134.129.98.47,134.129.98.48";
//            String clan_master = "";
//            configuration = HBaseConfiguration.create();
//            configuration.set("hbase.zookeeper.property.clientPort", clan_clientPort);
//            configuration.set("hbase.zookeeper.quorum", clan_quorum);// 设置zookeeper的主机
//            if (clan_master != null && !"".equals(clan_master)) {
//                configuration.set("hbase.master", clan_master);// 设置hbase的master主机名和端口
//            }
//            configuration.set("zookeeper.znode.parent", "/hbase-unsecure");
//            con = ConnectionFactory.createConnection(configuration);
//        } catch (Exception ex) {
//            log.error("初始化异常");
//            ex.printStackTrace();
//        }
//    }
//
//    /**
//     * 保存
//     */
//    public static void insertHbaseData(String rowKey, String val) throws Exception {
//        Table table = null;
//        //列簇
//        String colName = "DATA";
//        //表名
//        String tableName = "tbl_upload_file";
//        try {
//            table = con.getTable(TableName.valueOf(tableName));
//            Put put = new Put(Bytes.toBytes(rowKey));
//            put.add(Bytes.toBytes("f"), Bytes.toBytes("name"), Bytes.toBytes("Wongs"));
//            put.add(Bytes.toBytes("f"), Bytes.toBytes("ext"), Bytes.toBytes("Wongs"));
//            put.add(Bytes.toBytes("f"), Bytes.toBytes("author"), Bytes.toBytes("Wongs"));
////            put.add(Bytes.toBytes("f"), Bytes.toBytes("content"), file.getContent());
////            put.addColumn(Bytes.toBytes(colName), Bytes.toBytes(rowKey), Bytes.toBytes(val));
//            table.put(put);
//        } finally {
//            if (table != null) {
//                table.close();
//            }
//        }
//    }
//
//    /**
//     * 查询
//     */
//    public static String getData(String rowkey) throws IOException {
//        Map returnMap = new HashMap();
//        String colName = "DATA";
//        String tableName = "file_image_save";
//        Table table = null;
//        try {
//            table = con.getTable(TableName.valueOf(tableName));
//            Get get = new Get(Bytes.toBytes(rowkey));
//            get.addColumn(Bytes.toBytes(colName), Bytes.toBytes(rowkey));
//            Result r = table.get(get);
//            Cell[] cells = r.rawCells();
//            for (Cell cell : cells) {
//                returnMap.put(Bytes.toString(CellUtil.cloneQualifier(cell)), Bytes.toString(CellUtil.cloneValue(cell)));
//            }
//        } finally {
//            if (table != null) {
//                table.close();
//            }
//        }
//        return returnMap.get(rowkey) == null ? null : returnMap.get(rowkey).toString();
//    }
//}
