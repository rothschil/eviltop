package io.github.rothschil.concurrent;

public class CurrentDemo {

    private static int stat = 0;

    public static void main(String[] args) throws Exception{
        int counts = 100000;
        Thread t1 = new Thread(()->{
            for (int i = 0; i < counts; i++) {
                stat++;
            }
        });

        t1.start();
        Thread t2 = new Thread(()->{
            for (int i = 0; i < counts; i++) {
                stat--;
            }
        });
        t2.start();

        t1.join();
        t2.join();
        System.out.println("[stat] valus is " +stat);
    }
}
